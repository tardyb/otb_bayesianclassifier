
/*def d'une appli*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include <unordered_map>
#include <boost/functional/hash.hpp>
#include "otbBayesianClassifier.h"
#include "otbFunctorImageFilter.h"
#include "otbVariadicNamedInputsImageFilter.h"


namespace otb{
namespace Wrapper
{

class BayesianClassifier: public Application
{
public:
  typedef BayesianClassifier Self;
  typedef Application Superclass;
  
  typedef itk::SmartPointer<Self> Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Filters typedef for concatenation */
  typedef UInt32VectorImageType                VectorImageType;
  typedef UInt32ImageType                      IOLabelImageType;
  typedef IOLabelImageType::InternalPixelType  LabelPixelType;

  
  typedef otb::ImageList<IOLabelImageType> ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType,
  				       VectorImageType> ListConcatenerFilterType;
  
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType, LabelPixelType>               ExtractROIFilterType;
  //typedef ObjectList<ExtractROIFilterType>                                                            ExtractROIFilterListType;
  typedef std::unordered_map<std::pair<std::string,unsigned int>,double,
			     boost::hash<std::pair<std::string,unsigned int> > > 
                             MapOfKeysType;
  typedef std::vector<unsigned int> LabelVectorType;
 /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(BayesianClassifier, otb::Application);

  // change this
  //using FunctorType = otb::BinaryBayesianFunctorN<unsigned int, double, double>;
  // using FilterType = otb::BinaryBayesianFunctorImageFilterWithNBands<FloatVectorImageType, FloatVectorImageType, FunctorType>;
  //using BinaryBayesianFunctorType = BinaryBayesianFunctor<unsigned int, VectorImage<double>, VectorImage<double>>;
  //auto BinaryBayesianFunctorType = otb::VariadicInputsImageFilter<VectorImage<double>,VectorImage<double>,VectorImage<double>>::New();
  using BinaryBayesianFunctorType = BinaryBayesianFunctorN<unsigned int, LabelPixelType,LabelPixelType>;
  using UnaryTransitionFunctorType = UnaryTransitionFunctor<unsigned int, LabelPixelType>;
    
private:
  void DoInit()
  {
    SetName("BayesianClassifier");
    SetDescription("");

    AddParameter(ParameterType_InputImageList,"hsup","Supervised produced land cover map ");
    SetParameterDescription("hsup","Supervised produced land cover map in chronological order");
    MandatoryOff("hsup");
  
    AddParameter(ParameterType_InputImageList,"voters", "Naive land cover maps");
    SetParameterDescription("voters","Ordered by model's year");
    MandatoryOff("voters");
      
    AddParameter(ParameterType_String,"gfsm","Generalization FScore Matrix files");
    SetParameterDescription("gfsm","Generalization FSCORE Matrix, ordered by model's year, in csv format, a row by other history year, a col per class");
    MandatoryOff("gfsm");
      
    AddParameter(ParameterType_String,"tmatrix","Transition matrix file");
    SetParameterDescription("tmatrix","Transition matrix file, in csv format, a row per transition, a col per class, see ComputeTransitionMatrix");
    MandatoryOff("tmatrix");
      
    AddParameter(ParameterType_String,"priorprob","A CSV file containing per class a priori probabilities");
    SetParameterDescription("priorprob"," A CSV file containing per class a priori probabilities. Comments are allowed stating with # and one row, comma delimited values ");
    //SetDefaultParameterInt("priorprob",0);
    MandatoryOff("priorprob");
      
    AddParameter(ParameterType_InputImageList, "mask", "");
    SetParameterDescription("mask", "");

    AddParameter(ParameterType_OutputImage,"out","The output fused image");
    SetParameterDescription("out","The output map");

    AddParameter(ParameterType_String,"class","List of class file");
    SetParameterDescription("class","ascii file, one class id per line");

    AddRAMParameter();
  }

  void DoUpdateParameters()
  {
    
  }

  // faire un fichier d'utilitaire ?
  int CSVTransitionMatrixFileReader(const std::string fileName, MapOfKeysType& mapOfKey,LabelVectorType vectorRefLabels)
  {
    std::ifstream inFile;
    LabelVectorType vectorLabels;
    inFile.open(fileName.c_str());
    if(!inFile)
      {
	std::cerr << "Transition Matrix File opening problem with file:" << std::endl;
 	std::cerr << fileName.c_str() << std::endl;
 	return EXIT_FAILURE;
      }
    else
      {
	std::string currentLine, refLabelsLine, transLabelsLine, currentValue;
 	const char endCommentChar = ':';
 	const char separatorChar = ',';
 	const char eolChar = '\n';
 	std::getline(inFile, refLabelsLine, endCommentChar); // Skips the comments
 	std::getline(inFile, refLabelsLine, eolChar); // Gets the first line after the comment char until the End Of Line char
 	std::getline(inFile, transLabelsLine, endCommentChar); // Skips the comments
 	std::getline(inFile, transLabelsLine, eolChar); // Gets the second line after the comment char until the End Of Line char
	
 	std::istringstream issRefLabelsLine(refLabelsLine);
 	std::istringstream issTransLabelsLine(transLabelsLine);
 	
	
	// In the file the transition key are unique
	std::vector<std::string> vectorOfKey;
	while (issRefLabelsLine.good())
	  {
	    std::getline(issRefLabelsLine,currentValue,separatorChar);
	    std::string value = currentValue.c_str();
	    vectorOfKey.push_back(value);
	  }
	
	while (issTransLabelsLine.good())
	  {
	    std::getline(issTransLabelsLine,currentValue,separatorChar);
	    unsigned int value = std::atoi(currentValue.c_str());
	    vectorLabels.push_back(value);
	    auto it = find (vectorRefLabels.begin(), vectorRefLabels.end(), value);
	    if (it != vectorLabels.end())
	      std::cout << "Label found in nomenclature file: " << *it << '\n';
	    else
	      std::cout << "Label not found in nomenclature file: " << value << "\n";

	  }

	unsigned int nbLabels = vectorLabels.size();
	unsigned int nbKeys = vectorOfKey.size();
	
	//reading the file
	for(unsigned int itRow = 0; itRow < nbKeys; ++itRow)
	  {
	    std::getline(inFile,currentLine,eolChar);
	    std::istringstream issCurrentLine(currentLine);
	    for(unsigned int itCol = 0;itCol < nbLabels; ++itCol)
	      {
		std::pair<std::string,unsigned int> key = std::make_pair(vectorOfKey[itRow],vectorLabels[itCol]);
		std::getline(issCurrentLine,currentValue,separatorChar);
		mapOfKey[key] = static_cast<double> (std::atof(currentValue.c_str()));
	      }
	  }

	return EXIT_SUCCESS;
      }

  }

  auto ReadListOfClassFile(const std::string fileName, LabelToIndiceMapType &labelIndice, std::vector<unsigned int> &vectorLabels )
  {
    std::ifstream inFile(fileName);
    //inFile.open(fileName.c_str());
    if(!inFile)
      itkGenericExceptionMacro(<< "Could not open file " << fileName << "\n");
    std::string classLine{""};
    unsigned int idvect = 0;
    while(inFile>>classLine)
      {
	vectorLabels.push_back(std::stoi(classLine));
	labelIndice[std::stoi(classLine)] = idvect;
	idvect++;
      } 
    return EXIT_SUCCESS;
  }

  auto ReadProportionFile(const std::string filename)
  {
    
    std::ifstream inFile(filename);
    if(!inFile)
      itkGenericExceptionMacro(<< "Could not open file" << filename << "\n");
    std::vector<double> row;
    // init the weight for undecided label
    //row.push_back(1);
    std::string line;
    while(inFile >> line)
      { 
	//std::cout << line << std::endl;
	std::string delimiter = ",";
	std::string value = line.substr(line.find(delimiter)+delimiter.length(), line.length());
	//std::cout << value << std::endl;
	row.push_back(stof(value));
	// if(line.at(0) == '#')
	//   continue;
	// std::string val;
	// std::stringstream s (line);
	// while(getline(s, val, ','))
	//   {
	//     //std::cout << val << std::endl;
	//     row.push_back(stof(val));
	//   }
      }
    return row;
  }
  void DoExecute()
  {
    
    // Define all objects needed
    LabelVectorType vectorLabels;
    MapOfKeysType mapOfKey;
    int nbComponent = 0;
    
    
    
    LabelToIndiceMapType LabelToIndices;
    bool computeProp = true;
    ReadListOfClassFile(GetParameterString("class"), LabelToIndices, vectorLabels );

    unsigned int nbClass = vectorLabels.size();
    otbAppLogINFO("Number of class: " << nbClass-1 << " total readed: "<< nbClass);

    // test and implement proportion estimation
    
    // estimate proportion if needed
    std::vector<double> prop;
    if (HasValue("priorprob"))
      {
	 prop = ReadProportionFile(GetParameterString("priorprob"));
      }
    else
      {
	prop.clear();
	prop.resize(nbClass, 1);
    
      }

    
    // Load Transition Matrix
    if (HasValue("tmatrix") && HasValue("hsup"))
      {
	otbAppLogINFO("Reading Transition Matrix");
	std::string fileName = GetParameterString("tmatrix");

	CSVTransitionMatrixFileReader(fileName,mapOfKey,vectorLabels);
	otbAppLogINFO("Transition Matrix Loaded");
	//filter->SetMapOfKey(mapOfKey);
	
	FloatVectorImageListType::Pointer refList = GetParameterImageList("hsup");
	//Concatenate Supervised map
	std::cout << "Ref concat" << std::endl;
	m_RefList = ImageListType::New();
	m_ConcRef = ListConcatenerFilterType::New();
	for(unsigned int i=0; i<refList->Size();++i)
	  {
	    FloatVectorImageType::Pointer imRef = refList->GetNthElement(i);
	    imRef->UpdateOutputInformation();
	    ExtractROIFilterType::Pointer conv = ExtractROIFilterType::New();
	    conv->SetInput(imRef);
	    conv->SetChannel(1);
	    conv->UpdateOutputInformation();
	    m_ExtractorList.push_back(conv.GetPointer());
	    m_RefList->PushBack(conv->GetOutput());
	  }
	m_ConcRef->SetInput(m_RefList);
	nbComponent++;
	//filter->SetInput(1,m_ConcRef->GetOutput());
	//filter->SetUseOcsTransition(true);
      }
    

    //Concatenate Voters map
    if (HasValue("voters"))
      {
	FloatVectorImageListType::Pointer imageList = GetParameterImageList("voters");
	
	m_ImageList = ImageListType::New();
	m_Concatener = ListConcatenerFilterType::New();
	for(unsigned int i=0; i<imageList->Size(); ++i)
	  {
	    FloatVectorImageType::Pointer image = imageList->GetNthElement(i);
	    image->UpdateOutputInformation();
	    ExtractROIFilterType::Pointer converter = ExtractROIFilterType::New();
	    converter->SetInput(image);
	    converter->SetChannel(1);
	    converter->UpdateOutputInformation();
	    m_ExtractorList.push_back(converter.GetPointer());
	    m_ImageList->PushBack(converter->GetOutput());
	  }
	m_Concatener->SetInput(m_ImageList);
	nbComponent++;
	//filter->SetInput(0, m_Concatener->GetOutput());
	//filter->SetUseVoters(true);
      }

    // Create, execute and write output
    if (HasValue("voters") && HasValue("tmatrix"))
      {
	std::cout << "Fuse Voters and Transition \n";
	auto vote = NewFunctorFilter(BinaryBayesianFunctorType{prop, mapOfKey, nbClass, LabelToIndices, vectorLabels});
	vote->SetInput1(m_Concatener->GetOutput());
	vote->SetInput2(m_ConcRef->GetOutput());
	std::cout << "input given \n";
	vote->Update();
	
	std::cout << "End Update \n";
	SetParameterOutputImage("out", vote->GetOutput());
      }
    // else if (HasValue("voters"))
    //   {
    // 	std::cout << "Majority Voting  \n";
    //   }
    else if (HasValue("tmatrix"))
      {
	std::cout << "Predict with Transition \n";
	auto vote = NewFunctorFilter(UnaryTransitionFunctorType{prop, mapOfKey, nbClass, LabelToIndices, vectorLabels});
	vote->SetInput(m_ConcRef->GetOutput());
	vote->Update();
	SetParameterOutputImage("out",  vote->GetOutput());
      }
    //else if (computeProp)
    //  {
    //    std::cout << "Estimate with p \n";
    //  }
    else
      {
	itkExceptionMacro("No parameter detected! Please check them.");
      }
    // auto binaryF = NewFunctorFilter(m_binaryFunctor);
    // binaryF->SetInput1( m_Concatener->GetOutput()); //
    // binaryF->SetInput2(m_ConcRef->GetOutput());
    // binaryF->Update();


  }
  
  ListConcatenerFilterType::Pointer  m_Concatener;
  std::vector<itk::LightObject::Pointer>  m_ExtractorList;
  ListConcatenerFilterType::Pointer  m_ConcRef;
  ImageListType::Pointer             m_ImageList;
  ImageListType::Pointer             m_RefList;
  
  //FunctorType          m_binaryFunctor;
  //FilterType::Pointer                filter;
  // declarer les 9 filtres 
};//end of class BayesianFusion 
}//end of namespace wrapper
}//end of namespace otb
OTB_APPLICATION_EXPORT(otb::Wrapper::BayesianClassifier)


//  otbcli_BayesianClassifier  -hsup /mnt/data/data1/WORK/Transition/Images/Classif_2007_ST_2007_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2008_ST_2008_seed0_clipped_mask.tif -voters /mnt/data/data1/WORK/Transition/Images/Classif_2007_ST_2009_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2008_ST_2009_seed0_clipped_mask.tif -tmatrix /mnt/data/data2/WORK/Transition/TransitionSubsetMAX/tmp/Matrice_transition_2007_2008_seed_0.csv -mask /mnt/data/data1/WORK/Transition/DT/ShapeEmp/EmpCommune.tif -out /home/tardyb/Documents/tmp/testBaysien2entree.tif


//otbcli_BayesianClassifier  -hsup  /mnt/data/data1/WORK/Transition/Images/Classif_2008_ST_2008_seed0_clipped_mask.tif -voters /mnt/data/data1/WORK/Transition/Images/Classif_2007_ST_2009_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2008_ST_2009_seed0_clipped_mask.tif -tmatrix /mnt/data/data2/WORK/Transition/TransitionSubsetMAX/tmp/Matrice_transition_2007_2008_seed_0.csv -mask /mnt/data/data1/WORK/Transition/DT/ShapeEmp/EmpCommune.tif -out /home/tardyb/Documents/tmp/testBaysien2entree.tif -class /home/tardyb/Documents/DOC/doc_these/ListeClasse.txt

// otbcli_ComputeConfusionMatrix -in /home/tardyb/Documents/tmp/testBaysien2entree.tif -out /home/tardyb/Documents/tmp/testBaysien2entree.csv -ref vector -ref.vector.in /mnt/data/data2/WORK/DT_ShapeHarmo/DT_Harmo_sq/dt_so09clipped_seed0_val.sqlite -ref.vector.field class -ref.vector.nodata 200 -nodatalabel 200



// otbcli_BayesianClassifier  -hsup  /mnt/data/data1/WORK/Transition/Images/Classif_2008_ST_2008_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2009_ST_2009_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2010_ST_2010_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2011_ST_2011_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2012_ST_2012_seed0_clipped_mask.tif -voters /mnt/data/data1/WORK/Transition/Images/Classif_2007_ST_2013_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2008_ST_2013_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2009_ST_2013_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2010_ST_2013_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2011_ST_2013_seed0_clipped_mask.tif /mnt/data/data1/WORK/Transition/Images/Classif_2012_ST_2013_seed0_clipped_mask.tif -tmatrix /mnt/data/data2/WORK/Transition/TransitionSubsetMAX/tmp/Matrice_transition_2007_2008_2009_2010_2011_2012_seed_0.csv -mask /mnt/data/data1/WORK/Transition/DT/ShapeEmp/EmpCommune.tif -out /home/tardyb/Documents/tmp/testBaysien6entree.tif -class /home/tardyb/Documents/DOC/doc_these/ListeClasse.txt
