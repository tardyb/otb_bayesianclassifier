#ifndef __OTBBAYESIANCLASSIFIER_H
#define __OTBBAYESIANCLASSIFIER_H

#include <unordered_map>
#include <boost/functional/hash.hpp>
#include <numeric> //a enlever
namespace otb
{
  //typedef std::vector<unsigned int> LabelToIndiceMapType;
  using LabelToIndiceMapType = typename std::unordered_map<unsigned int, unsigned int>;
  
  using MapOfKeysType = typename std::unordered_map<std::pair<std::string, unsigned int> ,double, boost::hash<std::pair<std::string,unsigned int> > >;

  template<typename TIn>
  auto GetVotesProba(itk::VariableLengthVector<TIn> voters, unsigned int nbClass, LabelToIndiceMapType &labelToIndices)
  {
    std::vector<unsigned int>  VotersVector(nbClass, 0);
    std::vector<double>  ProbaVoters(nbClass, 0.0);
    for(size_t i = 0; i < voters.GetSize(); ++i)
      {
        VotersVector[labelToIndices[voters[i]]] ++;
      }
    std::transform(VotersVector.begin(),VotersVector.end(),ProbaVoters.begin(),std::bind2nd(std::divides<double>(),voters.GetSize()));
    return ProbaVoters;
  }

  template<typename TIn>
  auto GetTransitionProba(itk::VariableLengthVector<TIn> ref, MapOfKeysType &mapOfKey, unsigned int nbClasse, LabelToIndiceMapType &labelToIndices, std::vector<unsigned int> &vectorLabels)
  {
    
    std::vector<double> TransitionVector;
    TransitionVector.resize(nbClasse, 0.0);
    // Create reference key
    std::string refKey = "";
    for(size_t r=0;r<ref.Size();++r)
      {
        std::string refVal = std::to_string((unsigned int) ref[r]);
        for (size_t s = refVal.size();s<3;++s)
          refKey+='0';
        refKey+= refVal;
      }
    //std::cout << "ref key = " << refKey << std::endl;
    // Get the transition vector
    for(size_t t = 0; t<vectorLabels.size(); ++t)
      {
	
        std::pair<std::string, unsigned int> Key = std::make_pair(refKey,vectorLabels[t]);
        if (mapOfKey.find(Key) != mapOfKey.end())
          {
            TransitionVector[labelToIndices[vectorLabels[t]]] = mapOfKey[Key];
    	  
          }
    	else
    	  {
    	    //std::cout << "Key not found\n";
    	  }
        // else the proba value remains 0
      }
    // The transition matrix contains probabilities

    return TransitionVector;
  }
  
  template<typename TIn>
  auto getLabelFromVotesVector(const std::vector<TIn> &input, const std::vector<unsigned int> &ListOfClass)
  {

    // double sum = std::accumulate(input.begin(), input.end(), 0);
    //std::cout << sum << "\n";
    // if (sum != 0)
    //   {
	// for (auto v:input)
	//   {
	//     std::cout << v << " | ";
	//   }
	// std::cout << std::endl;
	// //      }
    unsigned int indice = std::distance(input.begin(), std::max_element(input.begin(), input.end()));
    return ListOfClass[indice];
  }
  
  
  template<typename TOut, typename TIn>
  struct UnaryTransitionFunctor
  {
    UnaryTransitionFunctor(std::vector<double> &p, MapOfKeysType &mapOfKey, unsigned int NbClass, LabelToIndiceMapType &LabelToIndices, std::vector<unsigned int> &ListOfClass)
    : m_p(p),
      m_NbClass(NbClass),
      m_MapOfKey(mapOfKey),
      m_LabelToIndices(LabelToIndices),
      m_ListOfClass(ListOfClass)
    {}

    auto operator()(const itk::VariableLengthVector<TIn> &in)
    {
    
      itk::VariableLengthVector<TOut> out(1);
      auto trans = GetTransitionProba(in, m_MapOfKey, m_NbClass, m_LabelToIndices, m_ListOfClass);
      std::vector<double> res(m_NbClass);
      std::transform(m_p.begin(), m_p.end(), trans.begin(), res.begin(), std::multiplies<double>());
      unsigned int label = getLabelFromVotesVector(res, m_ListOfClass);
      out[0] = static_cast<TOut>(label);
      return out;    }
    
    size_t OutputSize(...) const
    {
      return 1;
    }

    std::vector<double>       m_p;
    unsigned int              m_NbClass;
    MapOfKeysType             m_MapOfKey;
    // Allow use labels from 0 to N in vote processing
    LabelToIndiceMapType      m_LabelToIndices;
    std::vector<unsigned int> m_ListOfClass;
  };


  template<typename TOut, typename TIn>
  struct UnaryVotersFunctor
  {
    UnaryVotersFunctor(std::vector<double> &p, unsigned int NbClass, LabelToIndiceMapType &LabelToIndices, std::vector<unsigned int> &ListOfClass)
    : m_p(p),
      m_NbClass(NbClass),
      m_LabelToIndices(LabelToIndices),
      m_ListOfClass(ListOfClass)
    {}

    auto operator()(const itk::VariableLengthVector<TIn> &in) const
    {
      itk::VariableLengthVector<TOut> out(1);
      auto vote = GetVotesProba(in, m_NbClass, m_LabelToIndices);
      std::vector<double> res(m_NbClass);
      std::transform(vote.begin(), vote.end(), m_p.begin(), res.begin(), std::multiplies<double>());
      unsigned int label = getLabelFromVotesVector(res, m_ListOfClass);
      out[0] = static_cast<TOut>(label);
      return out;
    }
 
    size_t OutputSize(...) const
    {
      return 1;
    }
    
    std::vector<double> m_p;
    unsigned int              m_NbClass;
    // Allow use labels from 0 to N in vote processing
    LabelToIndiceMapType      m_LabelToIndices;
    std::vector<unsigned int> m_ListOfClass;
  };

  
template<typename TOut, typename TIn1, typename TIn2>
struct BinaryBayesianFunctorN
{
   //BinaryBayesianFunctorN(std::vector<double> p)
 
  BinaryBayesianFunctorN(std::vector<double> &p, MapOfKeysType &mapOfKey, unsigned int NbClass, LabelToIndiceMapType &LabelToIndices, std::vector<unsigned int> &ListOfClass)
    : m_p(p),
      m_NbClass(NbClass),
      m_MapOfKey(mapOfKey),
      m_LabelToIndices(LabelToIndices),
      m_ListOfClass(ListOfClass)
  { }
  
  auto operator()(const itk::VariableLengthVector<TIn1> &in1,
		  const itk::VariableLengthVector<TIn2> &in2)
  {
    itk::VariableLengthVector<TOut> out(1);
    auto vote = GetVotesProba(in1, m_NbClass, m_LabelToIndices);
    auto trans = GetTransitionProba(in2, m_MapOfKey, m_NbClass, m_LabelToIndices, m_ListOfClass);
    std::vector<double> res(m_NbClass);
    // if (m_p.size()>0)
    //  {
    std::vector<double> tmpVect(m_NbClass);
    std::transform(vote.begin(), vote.end(), trans.begin(), tmpVect.begin(), std::multiplies<double>());
    std::transform(tmpVect.begin(), tmpVect.end(), m_p.begin(), res.begin(), std::multiplies<double>());
    //  }
    //else
    //  {
    // std::transform(vote.begin(), vote.end(), trans.begin(), res.begin(), std::multiplies<double>());
    //  }
    // sortir max de res
    // unsigned int indice = std::distance(res.begin(), std::max_element(res.begin(), res.end()));
    unsigned int label = getLabelFromVotesVector(res, m_ListOfClass);
    out[0] = static_cast<TOut>(label);
    return out;
  }

  size_t OutputSize(...) const
  {
    return 1;
  }
  
  std::vector<double>       m_p;
  unsigned int              m_NbClass;
  MapOfKeysType             m_MapOfKey;
  // Allow use labels from 0 to N in vote processing
  LabelToIndiceMapType      m_LabelToIndices;
  std::vector<unsigned int> m_ListOfClass;
};


}
#endif
